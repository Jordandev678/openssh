#	$OpenBSD: u2fkeys.sh,v 1 2019/03/31 $
#	Placed in the Public Domain.

tid="u2f keys "

KEYS="$OBJ/authorized_keys_${USER}"

#Enable U2F authentication
#Disable RequireUserPresence for automatic tests
#TODO: Can we test with passwords enabled too?
SSHDOPTS="-o U2FAuthentication=yes -o AuthenticationMethods=u2f -o U2FRequireUserPresence=no"
SSHOPTS="-o U2FAuthentication=yes -o U2FRequireUserPresence=no"

U2FERR_TIMEOUT="fido_dev_make_cred() failed: FIDO_ERR_ACTION_TIMEOUT"
U2FERR_MISING="No U2F devices found"

##########
#Tests
##########
start_sshd $SSHDOPTS

mv ${KEYS} ${KEYS}.bak

#Test empty - should let us through
echo "" > $KEYS
${SSH} ${SSHOPTS} -F $OBJ/ssh_config somehost true \
	|| fail "SSH failed to connect with no keys"


#Test a fake key we wont have the counterpart for - should not let us through
echo "ssh-u2f AAAAB3NzaC11MmYAAABAmj7eRYrv8XnI+NhYn+ldASsd7EZW7yuQVLspK66IHhEOq2+MZCDWfXbOYUS3BNdg9umfc+BmPfrR4PkxI4RvXgAAAECxYwodHLvPXoBswmqNE3eteLhUQtJIhI1iEXbXEoiSdOzpex0pwCMqFoi2P1/5yPwpF1PnHNV52OXc/Lb52z+R my invalid U2F key" > $KEYS
${SSH} ${SSHOPTS} -F $OBJ/ssh_config somehost true \
	&& fail "SSH managed to connect with invalid keys"

#Register a key and test it
OUTPUT=$(${SSH} ${SSHOPTS} -o U2FMode=registration -F $OBJ/ssh_config somehost true)
if [ $? -eq 0 ]; then
	fail "SSH managed to connect with invalid keys"
fi

#Check we got a key
echo "$OUTPUT" | grep "ssh-u2f " > $KEYS
if [ $? -ne 0 ]; then
	echo "$OUTPUT" | grep -F "$U2FERR_TIMEOUT\n$U2FERR_MISSING"
	if [ $? -eq 1 ]; then
		warn "Could not register key to test U2F"
	else
		fail "SSH failed to register a U2F key"
	fi
else
	echo "$OUTPUT" | grep "ssh-u2f " > $KEYS || fail "U2F error"

	${SSH} ${SSHOPTS} -F $OBJ/ssh_config somehost true \
		|| fail "SSH failed to connect with a valid U2F key"
fi

mv ${KEYS}.bak ${KEYS}

