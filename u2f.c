#include "includes.h"
#include "config.h"

#ifdef U2F

#include <fido.h>
#include <unistd.h>

#include "u2f.h"
#include "ssherr.h"
#include "log.h"
#include "misc.h"
#include "digest.h"

fido_dev_t*
wait_for_u2f_devices(const char *devPath)
{
	fido_dev_t *chosenDev;
	fido_dev_info_t *devs = NULL;
	//const char *devPath;
	size_t devNum;
	time_t looking;
	int attempts = 0;
	int rc;

	if (!devPath){
		if ((devs = fido_dev_info_new(U2F_MAX_DEVLIST)) == NULL)
			fatal("fido_dev_info_new() failed");
		// The U2F implementation considerations recommend 3 seconds as
		// the time a client implementation should grant for security
		// keys to respond. We wait 3 times that for the user to insert
		// a security key (and it being detected).
		looking = monotime();
		do {
			if ((rc = fido_dev_info_manifest(devs, U2F_MAX_DEVLIST, &devNum)) != FIDO_OK)
				fatal("fido_dev_info_manifest() failed: %s", fido_strerr(rc));

			if (devNum < 1){
				if (attempts++ == 0)
					error("Please insert and touch your U2F security key.");
				usleep(50);
			}
		} while (devNum < 1 && (monotime() - looking) <= 9);
		if (devNum < 1)
			fatal("No U2F devices found. Did you plug in your U2F security key?");

		const fido_dev_info_t *di = fido_dev_info_ptr(devs, 0);
		devPath = fido_dev_info_path(di);
	}
	if ((chosenDev = fido_dev_new()) == NULL)
		fatal("fido_dev_new() failed");

	if ((rc = fido_dev_open(chosenDev, devPath)) != FIDO_OK)
		fatal("fido_dev_open() failed: %s", fido_strerr(rc));

	debug("Using CTAP%u device: %s",
	    (fido_dev_is_fido2(chosenDev)+1), devPath);

	//This frees the whole dev_info struct and everything from it we don't
	//explicitly copy, including all the info pointed to by
	//fido_dev_info_path etc.
	fido_dev_info_free(&devs, devNum);

	return chosenDev;
}

int
get_clientdata_sha256(u_char *dest, size_t destlen, char *chlg, size_t chlglen,
	char *sessionid, size_t sessionidlen)
{
	size_t digestsize = ssh_digest_bytes(SSH_DIGEST_SHA256);
	if(destlen < digestsize) return SSH_ERR_NO_BUFFER_SPACE;

	struct ssh_digest_ctx *ctx = ssh_digest_start(SSH_DIGEST_SHA256);
	ssh_digest_update(ctx, chlg, chlglen);
	ssh_digest_update(ctx, sessionid, sessionidlen);
	ssh_digest_final(ctx, dest, digestsize);

	return SSH_ERR_SUCCESS;
}

#endif
