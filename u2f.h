#ifndef OPENSSH_U2F_H
#define OPENSSH_U2F_H

#define U2F_CHALLENGE_LEN 32
#define U2F_PUBKEY_LEN 65
#define U2F_MODE_REGISTRATION 0
#define U2F_MODE_AUTHENTICATION 1

#define U2F_MAX_DEVLIST 64
#define U2F_MAX_KEYS 64
#define U2F_TOUCH_MSG "Please touch your U2F security key now."

#ifdef U2F

#include <fido.h>

fido_dev_t* wait_for_u2f_devices(const char *devPath);
int get_clientdata_sha256(u_char *dest, size_t destlen, char *chlg,
	size_t chlglen, char *sessionid, size_t sessionidlen);

#endif
#endif
