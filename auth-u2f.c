/*
 * Copyright (c) 2014 Google Inc.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

//TODO: They recently changed the packet_* functions format, this file has been updated to compile and test ok but the whole thing needs a once-over to validate they havn't made any other changes that have been missed

#include "includes.h"

#ifdef U2F

#include <ctype.h>
#include <openssl/x509.h>
#include <openssl/err.h>
#include <fido.h>
#include <fcntl.h>
#include <string.h>

#include "sshkey.h"
#include "hostfile.h"
#include "ssherr.h"
#include "auth.h"
#include "auth-options.h"
#include "ssh.h"
#include "ssh2.h"
#include "log.h"
#include "dispatch.h"
#include "misc.h"
#include "servconf.h"
#include "packet.h"
#include "digest.h"
#include "xmalloc.h"
#include "monitor_wrap.h"
#include "u2f.h"

/* import */
extern ServerOptions options;
extern u_char *session_id2;
extern u_int session_id2_len;

static int input_userauth_u2f_auth_response(int, u_int32_t, struct ssh *ssh);
static int input_userauth_u2f_register_response(int type, u_int32_t seq, struct ssh *ssh);

size_t
read_keysfile(FILE *fp, char *filename, struct passwd *pw, struct sshkey *keys[], size_t keyslen)
{
	char *line = NULL;
	size_t linesize = 0, keysidx = 0, linenum=0;
	struct sshkey *found = NULL;

	//TODO: This gives malformed or unparsable keys a pass
	//If no keys can be parsed they get authenticated - is that the
	//best behaviour?
	while (getline(&line, &linesize, fp) != -1) {
		++linenum;
		char *cp;
		found = sshkey_new(KEY_U2F);

		/* Skip leading whitespace, empty and comment lines. */
		for (cp = line; *cp == ' ' || *cp == '\t'; cp++)
			;
		if (!*cp || *cp == '\n' || *cp == '#')
			continue;

		if (sshkey_read(found, &cp) != 0) {
			continue;
		}
		if (found->type == KEY_U2F) {
			debug("ssh-u2f key found: file %s, line %zu", filename, linenum);
			keys[keysidx++] = found;
		}
	}
	return keysidx;
}

/*
 * Read n keys from file
 */
size_t
read_user_u2f_keys(struct passwd *pw, struct sshkey *keys[], size_t keyslen)
{
	size_t i, keyaridx = 0;
	for (i = 0; i < options.num_authkeys_files; i++) {
		FILE *fp;
		char *file;
		if (strcasecmp(options.authorized_keys_files[i], "none") == 0)
			continue;
		file = expand_authorized_keys(options.authorized_keys_files[i], pw);
		debug("looking for ssh-u2f keys in %s", file);
		if ((fp = fopen(file, "r")) == NULL) {
			free(file);
			continue;
		}
		size_t added = read_keysfile(fp, file, pw, &keys[keyaridx], keyslen-keyaridx);
		keyaridx += added;

		fclose(fp);
		free(file);

		if (keyaridx > keyslen+1)
			fatal("%s: Error, overflowed array", __func__);
		if (keyaridx > keyslen) break;
	}

	return keyaridx;
}

static int
userauth_u2f_register(struct ssh *ssh)
{
	int r;
	Authctxt *authctxt = ssh->authctxt;

	free(authctxt->u2f_challenge); //Should always be null but can't hurt
	authctxt->u2f_challenge = malloc(U2F_CHALLENGE_LEN);
	if(authctxt->u2f_challenge == NULL)
		fatal("malloc() failed for u2f_challenge");

	if((r = sshpkt_start(ssh, SSH2_MSG_USERAUTH_INFO_REQUEST)) != 0 ||
	   (r = sshpkt_put_string(ssh, authctxt->u2f_challenge, U2F_CHALLENGE_LEN)) != 0 ||
	   (r = sshpkt_put_u32(ssh, 0)) != 0 ||
	   (r = sshpkt_send(ssh)) != 0){
		error("Error sending u2f register packet");
		return r;
	}

	ssh_dispatch_set(ssh, SSH2_MSG_USERAUTH_INFO_RESPONSE,
		&input_userauth_u2f_register_response);
	authctxt->postponed = 1;
	return 0;
}

void
free_authctxt_u2f_keys(Authctxt *c)
{
	u_int32_t i;
	if (c->u2f_keys != NULL){
		for (i=0; i < c->u2f_keys_num; ++i){
			sshkey_free(c->u2f_keys[i]);
		}
		free(c->u2f_keys);
		c->u2f_keys_num = 0;
	}
}

static int
userauth_u2f_authenticate(struct ssh *ssh) // *authctxt)
{
	Authctxt *authctxt = ssh->authctxt;
	struct sshkey *keys[U2F_MAX_KEYS];
	size_t keysnum, i;
	int r;

	keysnum = PRIVSEP(read_user_u2f_keys(authctxt->pw, keys, U2F_MAX_KEYS));
	if (keysnum == 0) {
		char *reason = "Skipping U2F authentication: no ssh-u2f keys found in the authorized keys file(s).";
		debug("%s", reason);
		auth_debug_add("%s", reason);
		authctxt->postponed = 0;
		return 1;
	}else if (keysnum < 0){
		char *reason = "Terminating u2f authentication unsuccessfully, error reading authorized keys file(s)";
		error("%s", reason);
		auth_debug_add("%s", reason);
		userauth_finish(ssh, 0, "u2f", NULL);
		return 0;
	}

	free(authctxt->u2f_challenge); //TODO check free and error as below?
	if(authctxt->u2f_keys_num != 0 || authctxt->u2f_keys != NULL){
		//We can free it and continue on - but this is unexpected
		error("%s: u2f_keysnum was not empty", __func__);
		free_authctxt_u2f_keys(authctxt);
	}
	//Save keys to check against later
	authctxt->u2f_keys = malloc(keysnum * sizeof(struct sshkey *));
	if(authctxt->u2f_keys == NULL)
		fatal("%s: Error allocating u2f_keys array", __func__);
	memcpy(authctxt->u2f_keys, keys, keysnum * sizeof(struct sshkey *));
	authctxt->u2f_keys_num = keysnum;
	//Generate challenge
	authctxt->u2f_challenge = xmalloc(U2F_CHALLENGE_LEN);
	arc4random_buf(authctxt->u2f_challenge, U2F_CHALLENGE_LEN);

	if ((r = sshpkt_start(ssh, SSH2_MSG_USERAUTH_INFO_REQUEST)) != 0 ||
	    (r = sshpkt_put_string(ssh, authctxt->u2f_challenge, U2F_CHALLENGE_LEN)) != 0 ||
	    (r = sshpkt_put_u32(ssh, options.u2f_require_user_presence)) != 0 ||
	    (r = sshpkt_put_u32(ssh, keysnum)) != 0){
			error("Error sending u2f challenge request");
			userauth_finish(ssh, 0, "u2f", NULL);
			return 0;
	}
	for (i = 0; i<keysnum; ++i) //Send keys
		sshpkt_put_string(ssh, keys[i]->u2f_key_handle, keys[i]->u2f_key_handle_len);
	sshpkt_send(ssh);

	ssh_dispatch_set(ssh, SSH2_MSG_USERAUTH_INFO_RESPONSE,
		&input_userauth_u2f_auth_response);
	authctxt->postponed = 1;
	return 0;
}

static int
userauth_u2f(struct ssh *ssh) //Authctxt *authctxt)
{
	Authctxt *authctxt = ssh->authctxt;
	int mode, r;

	if ((r = sshpkt_get_u32(ssh, &mode)) != 0 ||
	    (r = sshpkt_get_end(ssh)) != 0)
		return 0;

	authctxt->u2f_challenge = NULL; //Need for both registration and auth

	if (mode == U2F_MODE_REGISTRATION) {
		debug("Starting U2F registration");
		return userauth_u2f_register(ssh);
	} else if (mode == U2F_MODE_AUTHENTICATION) {
		debug("Starting U2F authentication");
		authctxt->u2f_attempt = 0;
		//authctxt->u2f_key = NULL;
		return userauth_u2f_authenticate(ssh);
	} else {
		error("Unknown U2F mode %d requested by the client.", mode);
		return 0;
	}

	return 0;
}

static int
input_userauth_u2f_register_response(int type, u_int32_t seq, struct ssh *ssh)
{
	Authctxt *authctxt = ssh->authctxt;
	u_char *recvchlg, *sigptr, *authdataptr, *x509ptr;
	char *authfmtptr;
	size_t recvchlglen, siglen, authdatalen, x509len;
	fido_cred_t *cred = NULL;
	int rc;
	u_char digest[ssh_digest_bytes(SSH_DIGEST_SHA256)];

	authctxt->postponed = 0;

	if ((rc = sshpkt_get_string(ssh, &recvchlg, &recvchlglen)) != 0 ||
	    (rc = sshpkt_get_string(ssh, &sigptr, &siglen)) != 0 ||
	    (rc = sshpkt_get_string(ssh, &authdataptr, &authdatalen)) != 0 ||
	    (rc = sshpkt_get_cstring(ssh, &authfmtptr, NULL)) != 0 ||
	    (rc = sshpkt_get_string(ssh, &x509ptr, &x509len)) != 0 ||
	    (rc = sshpkt_get_end(ssh)) != 0)
		fatal("Error sending u2f registration response");

	if (recvchlglen < U2F_CHALLENGE_LEN)
		fatal("u2f challenge lengths differ - MITM?");

	if (memcmp(recvchlg, authctxt->u2f_challenge, U2F_CHALLENGE_LEN) != 0)
		fatal("u2f challenges differ - MITM?");

	if ((cred = fido_cred_new()) == NULL)
		fatal("fido_cred_new() failed");

	//libfido doesn't attempt to pick - need to set ourselves
	if ((rc = fido_cred_set_type(cred, COSE_ES256)))
		fatal("fido_cred_set_type() failed");

	//TODO: Check if there is any other fields should be included
	if ((rc = get_clientdata_sha256(digest, sizeof(digest), authctxt->u2f_challenge, U2F_CHALLENGE_LEN, session_id2, session_id2_len)) != SSH_ERR_SUCCESS)
		fatal("get_clientdata_sha256() failed: %s", ssh_err(rc));

	if ((rc = fido_cred_set_clientdata_hash(cred, digest, sizeof(digest))) != FIDO_OK)
		fatal("fido_cred_set_clientdata_hash() failed: %s", fido_strerr(rc));

	if ((rc = fido_cred_set_rp(cred, "SSHU2F", "U2F for SSH")) != FIDO_OK)
		fatal("fido_cred_set_rp() failed: %s", fido_strerr(rc));

	if ((rc = fido_cred_set_options(cred, 0, 0)) != FIDO_OK)
		fatal("fido_cred_set_options() failed: %s", fido_strerr(rc));

	if ((rc = fido_cred_set_authdata(cred, authdataptr, authdatalen)) != FIDO_OK)
		fatal("fido_cred_set_authdata() failed: %s", fido_strerr(rc));

	if ((rc = fido_cred_set_sig(cred, sigptr, siglen)) != FIDO_OK)
		fatal("fido_cred_set_sig() failed: %s", fido_strerr(rc));

	if ((rc = fido_cred_set_x509(cred, x509ptr, x509len)) != FIDO_OK)
		fatal("fido_cred_set_x509() failed: %s", fido_strerr(rc));

	if ((rc = fido_cred_set_fmt(cred, authfmtptr)) != FIDO_OK)
		fatal("fido_cred_set_fmt() failed: %s", fido_strerr(rc));

	/* From libfido2 manual, so it's clear what's going on here:
	 * Function verifies whether the client data hash, relying party ID,
	 * credential ID, and resident key and user verification attributes of
	 * cred have been attested by the holder of the private counterpart of
	 * the public key contained in the credential's x509 certificate.
         * Please note that the x509 certificate itself is not verified.
	 */
	if ((rc = fido_cred_verify(cred)) != FIDO_OK)
		fatal("fido_cred_verify() failed: %s", fido_strerr(rc));

	{
		int r;
		char *key64 = NULL, *authorizedkey;
		struct sshkey *sshkey = sshkey_new(KEY_U2F);
		sshkey->u2f_pubkey_len = fido_cred_pubkey_len(cred);
		sshkey->u2f_key_handle_len = fido_cred_id_len(cred);

		sshkey->u2f_pubkey = malloc(sshkey->u2f_pubkey_len);
		if (sshkey->u2f_pubkey == NULL) fatal("u2f_pubkey malloc failed");
		memcpy(sshkey->u2f_pubkey, fido_cred_pubkey_ptr(cred), sshkey->u2f_pubkey_len);

		sshkey->u2f_key_handle = malloc(sshkey->u2f_key_handle_len);
		if (sshkey->u2f_key_handle == NULL) fatal("u2f_key_handle malloc failed");
		memcpy(sshkey->u2f_key_handle, fido_cred_id_ptr(cred), sshkey->u2f_key_handle_len);

		if ((r = sshkey_to_base64(sshkey, &key64)) != 0)
			error("%s: sshkey_to_base64 failed: %s", __func__, ssh_err(r));
		xasprintf(&authorizedkey, "ssh-u2f %s my security key", key64);

		//Lets double check it comes back as it left
		//TODO: Put into test?
		/*
		char *cp = authorizedkey;
		struct sshkey *reloadedKey = sshkey_new(KEY_U2F);
		if((r = sshkey_read(reloadedKey, &cp)) != 0)
			fatal("%s: sshkey_read failed: %s", __func__, ssh_err(r));
		if(sshkey->u2f_key_handle_len != reloadedKey->u2f_key_handle_len)
			fatal("%s: sshkey did not reload: key_handle_len", __func__);
		if(memcmp(sshkey->u2f_key_handle, reloadedKey->u2f_key_handle, reloadedKey->u2f_key_handle_len) != 0)
			fatal("%s: sshkey did not reload: key_handle", __func__);
		if(sshkey->u2f_pubkey_len != reloadedKey->u2f_pubkey_len)
			fatal("%s: sshkey did not reload: u2f_pubkey_len", __func__);
		if(memcmp(sshkey->u2f_pubkey, reloadedKey->u2f_pubkey, reloadedKey->u2f_pubkey_len) != 0)
			fatal("%s: sshkey did not reload: pubkey", __func__);
		*/

		if ((r = sshpkt_start(ssh, SSH2_MSG_USERAUTH_INFO_REQUEST)) != 0 ||
		    (r = sshpkt_put_cstring(ssh, authorizedkey)) != 0 ||
		    (r = sshpkt_put_u32(ssh, 0)) != 0 ||
		    (r = sshpkt_send(ssh)) != 0)
			fatal("Error senind authorized u2f key");

		free(authorizedkey);
		free(key64);
		sshkey_free(sshkey);
		ssh_dispatch_set(ssh, SSH2_MSG_USERAUTH_INFO_RESPONSE, NULL);
	};

	//Free's everything under the cred we didn't explcitly copy
	fido_cred_free(&cred);

	//Won't be needing this anymore - shouldn't be reused
	free(authctxt->u2f_challenge);

	//Received strings from packet
	free(recvchlg);
	free(sigptr);
	free(authdataptr);
	free(authfmtptr);
	free(x509ptr);

	userauth_finish(ssh, 0, "u2f", NULL);
	return rc;
}

int
verify_u2f_user(struct sshkey *key, u_char *dgst, size_t dgstlen, u_char *sigptr, size_t siglen, u_char *authdataptr, size_t authdatalen, int upFlag)
{
	int authorised = 0;
	int rc;
	fido_assert_t *assert = NULL;

	if ((assert = fido_assert_new()) == NULL){
		error("fido_assert_new() failed");
		goto out;
	}

	if((rc = fido_assert_set_clientdata_hash(assert, dgst, dgstlen)) != FIDO_OK){
		error("fido_assert_set_clientdata_hash() failed: %s", fido_strerr(rc));
		goto out;
	}

	if((rc = fido_assert_set_rp(assert, "SSHU2F")) != FIDO_OK){
		error("fido_assert_set_rp() failed: %s", fido_strerr(rc));
		goto out;
	}

	if((rc = fido_assert_set_count(assert, 1)) != FIDO_OK){
		error("fido_assert_set_count() failed: %s", fido_strerr(rc));
		goto out;
	}

	if((rc = fido_assert_set_authdata(assert, 0, authdataptr, authdatalen)) != FIDO_OK){
		error("fido_assert_set_count() failed: %s", fido_strerr(rc));
		goto out;
	}

	if((rc = fido_assert_set_options(assert, upFlag, 0)) != FIDO_OK){
		error("fido_assert_set_options() failed: %s", fido_strerr(rc));
		goto out;
	}

	if((rc = fido_assert_set_sig(assert, 0, sigptr, siglen)) != FIDO_OK){
		error("fido_assert_set_sig() failed: %s", fido_strerr(rc));
		goto out;
	}

	if((rc = fido_assert_verify(assert, 0, COSE_ES256, key->u2f_pubkey)) != FIDO_OK){
		error("fido_assert_verify() failed: %s", fido_strerr(rc));
		if(rc == FIDO_ERR_INVALID_PARAM)
			debug("fido_assert_verify() == FIDO_ERR_INVALID_PARAM"
			    " usually means the client didn't fulfill our"
			    " authorisation requirements e.g. no user presence");
		goto out;
	}

	authorised = 1;
	debug("U2F authentication signature verified.");

out:
	fido_assert_free(&assert);
	return authorised;
}

static int
input_userauth_u2f_auth_response(int type, u_int32_t seq, struct ssh *ssh)
{
	int authenticated = 0;
	Authctxt *authctxt = ssh->authctxt;
	int numasserts;
	int i, rc;
	int upFlag;
	u_int j;
	u_char digest[ssh_digest_bytes(SSH_DIGEST_SHA256)];

	//Get packet data
	if ((rc = sshpkt_get_u32(ssh, &upFlag)) != 0 ||
	    (rc = sshpkt_get_u32(ssh, &numasserts)) != 0)
		fatal("Invalid u2f response packet");

	upFlag = upFlag || options.u2f_require_user_presence;

	//Validate
	if ((rc = get_clientdata_sha256(digest, sizeof(digest), authctxt->u2f_challenge, U2F_CHALLENGE_LEN, session_id2, session_id2_len)) != SSH_ERR_SUCCESS)
		fatal("get_clientdata_sha256() failed: %s", ssh_err(rc));

	for (i=0; i<numasserts && !authenticated; ++i){
		u_char *authdataptr, *sigptr;
		size_t authdatalen, siglen;

		if ((rc = sshpkt_get_string(ssh, &sigptr, &siglen)) != 0 ||
		    (rc = sshpkt_get_string(ssh, &authdataptr, &authdatalen)) != 0)
			fatal("Error getting u2f keys from packet");

		//TODO: Update to send all keys at once (i.e. remove loop))
		for (j=0; j<authctxt->u2f_keys_num && !authenticated; j++){
			authenticated = PRIVSEP(verify_u2f_user( authctxt->u2f_keys[j],
			    digest, sizeof(digest), sigptr, siglen, authdataptr,
			    authdatalen, upFlag));
		}

		free(sigptr);
		free(authdataptr);
	}

	if ((rc = sshpkt_get_end(ssh)) != 0)
		fatal("Error parsing u2f packet");

	authctxt->postponed = 0;
	ssh_dispatch_set(ssh, SSH2_MSG_USERAUTH_INFO_RESPONSE, NULL);

	authctxt->u2f_attempt++;
	if (authenticated) {
		userauth_finish(ssh, 1, "u2f", NULL);
	} else {
		// Try again, perhaps there are more keys to use.
		//userauth_u2f_authenticate(ssh);
		userauth_finish(ssh, 0, "u2f", NULL);
	}

	return 0;
}

Authmethod method_u2f = {
	"u2f",
	userauth_u2f,
	&options.u2f_authentication
};

#endif /* U2F */
